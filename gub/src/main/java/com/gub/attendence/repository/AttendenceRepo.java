package com.gub.attendence.repository;

import com.gub.attendence.model.Attendence;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

public interface AttendenceRepo extends CrudRepository<Attendence, Integer> {

}
