package com.gub.attendence.service;

import com.gub.attendence.model.Attendence;
import com.gub.attendence.repository.AttendenceRepo;
import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AttendenceService {
    @Autowired
    AttendenceRepo attendenceRepo;

    public void createAttendence(){

        Attendence attendence = new Attendence();
        attendence.setCourse_name("Algorithm and Data Structure");
        attendence.setSection("A");
        attendence.setSemester("Spring");
        attendenceRepo.save(attendence);
        Attendence attendence1 = new Attendence();
        attendence1.setCourse_name("Human Resource Management");
        attendence1.setSection("B");
        attendence1.setSemester("Summer");
        attendenceRepo.save(attendence1);
        Attendence attendence2 = new Attendence();
        attendence2.setCourse_name("Discrete Mathematics");
        attendence2.setSection("B");
        attendence2.setSemester("Spring");
        attendenceRepo.save(attendence2);
        Attendence attendence3 = new Attendence();
        attendence3.setCourse_name("Artificial Intelligence");
        attendence3.setSection("A");
        attendence3.setSemester("Winter");
        attendenceRepo.save(attendence3);

    }

    public List<Attendence> showAttendence(){
        Iterable<Attendence> iterableAttendence = attendenceRepo.findAll();
        List<Attendence> attendenceList = new ArrayList<>();
        for (Attendence attendence: iterableAttendence) {
            attendenceList.add(attendence);
        }
        return attendenceList;
    }
}
