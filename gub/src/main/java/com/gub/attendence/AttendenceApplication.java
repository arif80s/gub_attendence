package com.gub.attendence;

import com.gub.attendence.controller.AttendenceController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = "com.gub.attendence")
@EnableJpaRepositories(basePackages = "com.gub.attendence.repository")
public class AttendenceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AttendenceApplication.class, args);
    }
}
