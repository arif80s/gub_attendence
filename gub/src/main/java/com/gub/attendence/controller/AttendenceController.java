package com.gub.attendence.controller;

import com.gub.attendence.model.Attendence;
import com.gub.attendence.service.AttendenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/attendence")
public class AttendenceController {

    @Autowired
     private AttendenceService attendenceService;

    @GetMapping("/create")
    @ResponseBody
    public String createAttendence(){
        attendenceService.createAttendence();
        return "attendence object created in the database !!!!";

    }

    @GetMapping("/showAll")
    @ResponseBody
    public List<Attendence> showAttendence(){
        return attendenceService.showAttendence();
    }



}
