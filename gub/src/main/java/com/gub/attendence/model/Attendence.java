package com.gub.attendence.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity(name = "attendence")
public class Attendence implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int student_id;
    private String course_name;
    private String section;
    private  String semester;

    public Attendence() {
    }

    public Attendence(int student_id, String course_name, String section, String semester) {
        this.student_id = student_id;
        this.course_name = course_name;
        this.section = section;
        this.semester = semester;
    }

    public int getStudent_id() {
        return student_id;
    }

    public String getCourse_name() {
        return course_name;
    }

    public String getSection() {
        return section;
    }

    public String getSemester() {
        return semester;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }
}
